<?php

if(isset($_POST['username'])) {

    $inputUser = htmlspecialchars($_POST['username']);
    $inputPass = htmlspecialchars($_POST['password']);
    $inputPas2 = htmlspecialchars($_POST['repeat']);
    $inputMail = htmlspecialchars($_POST['email']);

    $firstname = htmlspecialchars($_POST['firstname']);
    $lastname = htmlspecialchars($_POST['lastname']);
    $telephone = htmlspecialchars($_POST['telephone']);
    $country = htmlspecialchars($_POST['country']);
    $province = htmlspecialchars($_POST['province']);
    $city = htmlspecialchars($_POST['city']);
    $address = htmlspecialchars($_POST['address']);
    $postalcode = htmlspecialchars($_POST['postalcode']);

    //Validate input
    $errors = [];

    if(strlen($inputPass) < 8) {
      array_push($errors, "Password should be at least 8 characters!");
    }

    if($inputPass != $inputPas2) {
      array_push($errors, "Passwords do not match!");
    }

    if(count($errors) > 0) {
      foreach($errors as $error) {
        App::addError($error);
      }
    } else {
      //Register user
      $user = User::register($inputUser, $inputMail, $inputPass, 'user', $firstname, $lastname, $telephone, $country, $province, $city, $address, $postalcode);
      if($user) {
          echo $user->getUsername();

          App::redirect("home");
      }
    }
}
?>
<div class="container">
    <h1>
        REGISTER
    </h1>

    <form action="?page=register" method="POST">

        <input type="text" placeholder="Username" required name="username"/><br/>
        <input type="email" placeholder="E-mail" required name="email"/><br/>
        <input type="password" placeholder="Password" required name="password"/><br/>
        <input type="password" placeholder="Repeat" required name="repeat"/><br/>

        <input type="text" placeholder="firstname" required name="firstname"/><br/>
        <input type="text" placeholder="lastname" required name="lastname"/><br/>
        <input type="text" placeholder="telephone" required name="telephone"/><br/>
        <input type="text" placeholder="country" required name="country"/><br/>
        <input type="text" placeholder="province" required name="province"/><br/>
        <input type="text" placeholder="city" required name="city"/><br/>
        <input type="text" placeholder="address" required name="address"/><br/>
        <input type="text" placeholder="postalcode" required name="postalcode"/><br/>

        <input type="submit" value="Register"/>
    </form>

</div>
