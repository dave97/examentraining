<?php

if(isset($_POST['name'])){

	//Validate input
	$inputName = htmlspecialchars($_POST['name']);
	$inputDescription = htmlspecialchars($_POST['description']);

	$product = Categorie::register($inputName, $inputDescription);
}

?>
<div class="container">
    <h1>
        ADD CATEGORIE
    </h1>

<?php 

	$form = Categorie::addCategorieForm()->getHTML(); 
	echo $form;
?>

</div>