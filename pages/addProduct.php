<?php

if(isset($_POST['name'])){

	//Validate input
	$inputName = htmlspecialchars($_POST['name']);
	$inputBrand = htmlspecialchars($_POST['brand']);
	$inputModel = htmlspecialchars($_POST['model']);
	$inputEan = htmlspecialchars($_POST['ean']);
	$inputPrice = filter_var($_POST['price'], FILTER_SANITIZE_NUMBER_FLOAT,
FILTER_FLAG_ALLOW_THOUSAND);

	$product = Product::register($inputName, $inputBrand, $inputModel, $inputEan, $inputPrice);
}

?>
<div class="container">
    <h1>
        ADD PRODUCT
    </h1>

<?php 

	$form = Product::addProductForm()->getHTML(); 
	echo $form;
?>

</div>