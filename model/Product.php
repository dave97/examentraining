	<?php
	
	class Product extends Model{

    // Define properties here
    // Define them as protected! Example:
    // protected $brand;

    // protected $id;
    protected $name;
    protected $brand;
    protected $model; // maybe choose another name for this?
    protected $ean;
    protected $price;

    // The constructor should not have any arguments. 
    // If you do need them, make sure it could be called without arguments.
    public function __construct(){

    }

    public function getCategories(){
        return $this->hasMany('Categorie');
    }

    public static function register($name, $brand, $model, $ean, $price) {
        $product = new Product();
        $product->name = $name;
        $product->brand = $brand;
        $product->model = $model;
        $product->ean = $ean;
        $product->price = $price;

        if ($product->save()) {
            return $product;
        } else {
            return false;
        }
    }

    public static function addProductForm(){
        $form = new Form();

        $form->addField((new FormField("name"))->type("text")->placeholder("Name"));
        $form->addField((new FormField("brand"))->type("text")->placeholder("Brand"));
        $form->addField((new FormField("model"))->type("text")->placeholder("Model"));
        $form->addField((new FormField("ean"))->type("text")->placeholder("EAN"));
        $form->addField((new FormField("price"))->type("text")->placeholder("Price"));

        return $form;
    }

    // In this function you can check if an "about to be saved" object meets 
    // the requirements. If not, return false. If so, return true.
    public static function newModel($obj){
        return true;    
    }

}