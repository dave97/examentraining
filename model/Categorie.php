	<?php
	
	class Categorie extends Model{

    // Define properties here
    // Define them as protected! Example:
    // protected $brand;

    // protected $id;
    protected $name;
    protected $description;
    
    // The constructor should not have any arguments. 
    // If you do need them, make sure it could be called without arguments.
    public function __construct(){

    }

    public static function register($name, $description) {
        $categorie = new Categorie();
        $categorie->name = $name;
        $categorie->description = $description;

        if ($categorie->save()) {
            return $categorie;
        } else {
            return false;
        }
    }

    public static function addCategorieForm(){
        $form = new Form();

        $form->addField((new FormField("name"))->type("text")->placeholder("Name"));
        $form->addField((new FormField("description"))->type("text")->placeholder("Description"));

        return $form;
    }

    // In this function you can check if an "about to be saved" object meets 
    // the requirements. If not, return false. If so, return true.
    public static function newModel($obj){
        return true;    
    }

}