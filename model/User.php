<?php

class User extends Model {

    //protected $id;
    protected $username;
    protected $email;
    protected $password;
    protected $salt;
    protected $role;

    protected $firstname;
    protected $lastname;
    protected $telephone;
    protected $country;
    protected $province;
    protected $city;
    protected $address;
    protected $postalcode;

    public function __construct($username = "")
    {
        if($username != "") {
            $this->username = $username;
        }
    }

    public static function login($username, $password) {
        $res = User::findBy('username', $username);

        if (count($res) > 0) {
            $user = $res[0];

            if ($user->checkPassword($password)) {
                App::setLoggedInUser($user);
                return $user;
            }
        }
        return false;
    }

    public static function register($username, $email, $password, $role, $firstname, $lastname, $telephone, $country, $province, $city, $address, $postalcode) {
        $user = new User($username);
        $user->email = $email;
        $user->role = $role;
        $user->setPassword($password);

        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->telephone = $telephone;
        $user->country = $country;
        $user->province = $province;
        $user->city = $city;
        $user->address = $address;
        $user->postalcode = $postalcode;

        if ($user->save()) {
            App::setLoggedInUser($user);
            return $user;
        } else {
            return false;
        }
    }

    private function setPassword($password)
    {
        $this->salt = self::generateSalt();
        $this->password = hash('sha256', $password . $this->salt);
    }

    public static function generateSalt()
    {
        return uniqid();
    }

    public static function getLoginForm()
    {

        $form = new Form();
        $form->addField(new FormField("username", "text", "username"));
        return $form->getHTML();

    }

    protected static function newModel($obj)
    {

        $email = $obj->email;

        $existing = User::findBy('email', $email);
        if(count($existing) > 0) return false;

        //Check if user is valid
        return true;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail()
    {
        return $this->email;
    }


    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPostalcode()
    {
        return $this->postalcode;
    }
    
    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {

        $possible = ['user', 'admin', 'customer'];
        if (array_search($role, $possible)) {
            $this->role = $role;
            return true;
        }
        return false;
    }

    private function checkPassword($password)
    {
        $hash = hash('sha256', $password . $this->salt);
        return ($hash == $this->password);
    }
}
