<?php
//Create your menu here
?>
<nav>
    <div>
        <?php if($_SESSION['role'] == "user" || $_SESSION['role'] == "admin") { ?>
		<ul class="left">
			<li><a href="?page=home">home</a></li>
		</ul>
        <?php } ?>
        <ul class="right">
            <?php if($_SESSION['role'] == "guest") { ?>
			<li>
				<a href="?page=login">login</a>
			</li>
			<li>
				<a href="?page=register">register</a>
			</li>
            <?php } else { ?>
            <?php if($_SESSION['role'] == "admin") { ?>
			<li>
				<a href="?page=addProduct">add product</a>
			</li>
			<li>
				<a href="?page=addCategorie">add categorie</a>
			</li>
			<?php } ?>
			<li>
				<a href="?page=account">account</a>
			</li>
			<li>
				<a href="?page=logout">logout</a>
			</li>
            <?php } ?>
        </ul>
    </div>
</nav>