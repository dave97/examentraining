<?php
if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);

    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}
if (PHP_VERSION_ID < 70000) {
    echo FrameworkException::message("Your PHP version is too low. Use at least PHP 7");
}
?>
<title>Dendrowen framework</title>
<link rel="stylesheet" href="css/main.css"/>
<link rel="stylesheet" href="css/header.css"/>
<link rel="stylesheet" href="css/exception.css"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script
  src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script
  src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
